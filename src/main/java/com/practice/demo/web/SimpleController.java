package com.practice.demo.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@CrossOrigin("*")
public class SimpleController {

    @GetMapping("/test")
    public ResponseEntity getHelloWorld() {
        return ResponseEntity.ok("Hello World!");
    }
}
