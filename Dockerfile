FROM openjdk:8-jre-alpine
ADD build/libs/demo-0.0.1-SNAPSHOT.jar HelloWorld.jar
EXPOSE 8080
CMD java -jar HelloWorld.jar
